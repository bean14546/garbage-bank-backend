const mongoose = require('mongoose')
const { DB_HOST, DB_PORT, DB_NAME } = process.env
const mongoURL = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`

mongoose.connect(mongoURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log('Connected to MongoDB Complete')
}).catch((error) => {
  console.error('Connected to MongoDB Fail', error)
})

module.exports = mongoose