const { TransactionHistoryModel } = require('../models/transactionHistory')
const responseHandlers = require('./utils/response')

const _find = async (req, res, next) => {
  try {
    const transactionHistory = await TransactionHistoryModel.find().populate([
      { path: 'user', select: '-password' },
      {
        path: 'transaction',
        populate: {
          path: 'garbages',
          populate: 'garbage_category'
        }
      }
    ])

    return responseHandlers.success(res, transactionHistory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _findByID = async (req, res, next) => {
  try {
    const transactionHistory = await TransactionHistoryModel.findById(req.params.id).populate([
      { path: 'user', select: '-password' },
      {
        path: 'transaction',
        populate: { path: 'garbages' }
      }
    ])

    if (!transactionHistory) return responseHandlers.notFound(res, 'ไม่พบ transactionHistory')

    return responseHandlers.success(res, transactionHistory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _create = async (req, res, next) => {
  try {
    const transactionHistory = await TransactionHistoryModel.create({ ...req.body })

    return responseHandlers.created(res, transactionHistory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _update = async (req, res, next) => {
  try {
    const transactionHistory = await TransactionHistoryModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { ...req.body } },
      { new: true }
    )

    if (!transactionHistory) return responseHandlers.notFound(res, 'ไม่พบ transactionHistory')

    return responseHandlers.success(res, transactionHistory)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _delete = async (req, res, next) => {
  try {
    await TransactionHistoryModel.findByIdAndDelete(req.params.id)

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

module.exports = {
  _find,
  _findByID,
  _create,
  _update,
  _delete
}
