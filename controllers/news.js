const { NewsModel } = require('../models/news')
const responseHandlers = require('./utils/response')

const _find = async (req, res, next) => {
  try {
    const carousels = await NewsModel.find()

    return responseHandlers.success(res, carousels)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _findByID = async (req, res, next) => {
  try {
    const news = await NewsModel.findById(req.params.id)

    if (!news) return responseHandlers.notFound(res, 'ไม่พบข่าว')

    return responseHandlers.success(res, news)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _create = async (req, res, next) => {
  try {
    const news = await NewsModel.create({ ...req.body })

    return responseHandlers.created(res, news)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _update = async (req, res, next) => {
  try {
    const news = await NewsModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { ...req.body } },
      { new: true }
    )

    if (!news) return responseHandlers.notFound(res, 'ไม่พบข่าวหรือประกาศ')

    return responseHandlers.success(res, news)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _delete = async (req, res, next) => {
  try {
    await NewsModel.findByIdAndDelete(req.params.id)

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

module.exports = {
  _find,
  _findByID,
  _create,
  _update,
  _delete
}