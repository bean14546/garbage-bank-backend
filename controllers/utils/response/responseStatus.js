module.exports = {
  ok: 200,
  created: 201,
  badRequest: 400,
  unAuthorized: 401,
  notFound: 404,
  conflict: 409,
  internalServerError: 500
}