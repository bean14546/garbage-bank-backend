const responseStatus = require('./responseStatus')
const responseMessage = require('./responseMessage')
const responseHandlers = {
  success: (res, data, message = responseMessage.success) => {
    return res.status(responseStatus.ok).json({ success: true, message, data })
  },

  created: (res, data, message = responseMessage.created) => {
    return res.status(responseStatus.created).json({ success: true, message, data })
  },

  badRequest: (res, message = responseMessage.badRequest) => {
    return res.status(responseStatus.badRequest).json({ success: false, message })
  },

  unAuthorized: (res, message = responseMessage.unAuthorized) => {
    return res.status(responseStatus.unAuthorized).json({ success: false, message })
  },

  notFound: (res, message = responseMessage.notFound) => {
    return res.status(responseStatus.notFound).json({ success: false, message })
  },

  conflict: (res, message = responseMessage.conflict) => {
    return res.status(responseStatus.conflict).json({ success: false, message })
  },

  serverError: (res, message = responseMessage.internalServerError) => {
    return res.status(responseStatus.internalServerError).json({ success: false, message })
  },

  catchError: (res, error) => {
    return res.status(responseStatus.internalServerError).json({ success: false, message: error.message })
  }
}

module.exports = responseHandlers
