const { UserModel } = require('../../models/user')
const responseHandlers = require('../utils/response')

const _find = async (req, res, next) => {
  try {
    const users = await UserModel.find().select('-password')

    return responseHandlers.success(res, users)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _findByID = async (req, res, next) => {
  try {
    const user = await UserModel.findById(req.params.id).select('-password')

    if (!user) return responseHandlers.notFound(res, 'ไม่พบผู้ใช้')

    return responseHandlers.success(res, user)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _update = async (req, res, next) => {
  try {
    const user = await UserModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { ...req.body } },
      { new: true }
    )

    if (!user) return responseHandlers.notFound(res, 'ไม่พบผู้ใช้')

    return responseHandlers.success(res, user)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _delete = async (req, res, next) => {
  try {
    await UserModel.findByIdAndDelete(req.params.id)

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

module.exports = {
  _find,
  _findByID,
  _update,
  _delete
}