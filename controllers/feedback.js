const { FeedbackModel } = require('../models/feedback')
const responseHandlers = require('./utils/response')

const _find = async (req, res, next) => {
  try {
    const feedback = await FeedbackModel.find().populate([
      { path: 'user', select: '-password' } // ใส่ขีด - ด้านหน้าฟิลด์ที่ไม่ต้องการให้ดึงมาด้วย
    ])

    return responseHandlers.success(res, feedback)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _findByID = async (req, res, next) => {
  try {
    const feedback = await FeedbackModel.findById(req.params.id).populate([
      { path: 'user', select: '-feedback' }
    ])

    if (!feedback) return responseHandlers.notFound(res, 'ไม่พบ feedback')

    return responseHandlers.success(res, feedback)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _create = async (req, res, next) => {
  try {
    const feedback = await FeedbackModel.create({ ...req.body })

    return responseHandlers.created(res, feedback)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _update = async (req, res, next) => {
  try {
    const feedback = await FeedbackModel.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { ...req.body } },
      { new: true }
    )

    if (!feedback) return responseHandlers.notFound(res, 'ไม่พบ feedback')

    return responseHandlers.success(res, feedback)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

const _delete = async (req, res, next) => {
  try {
    await FeedbackModel.findByIdAndDelete(req.params.id)

    return responseHandlers.success(res)
  } catch (error) {
    return responseHandlers.catchError(res, error)
  }
}

module.exports = {
  _find,
  _findByID,
  _create,
  _update,
  _delete
}
