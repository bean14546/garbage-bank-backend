const jwt = require('jsonwebtoken')
const { PersonalAccessTokenModel } = require('../models/personalAccessToken')
const { APP_KEY } = process.env

// ตรวจสอบว่ามี token ส่งมาหรือไม่ หมดอายุหรือยัง มีรูปแบบที่ถูกต้องหรือไม่
async function verifyToken (req, res, next) {
  try {
    const token = req.headers.authorization ? req.headers.authorization.split('Bearer ')[1] : false
    const personalAccessToken = await PersonalAccessTokenModel.findOne({ token })
    const isTokenExpired = personalAccessToken.expired
    if (token && !isTokenExpired) {
      const decoded = jwt.verify(token, APP_KEY)
      req.auth = { decoded, tokenID: personalAccessToken._id }
      return next()
    } else {
      return res.status(404).json({ success: false, message: 'You are not logged in or Token may have expired.' })
    }
  } catch (error) {
    return res.status(401).json({ success: false, message: 'Authentication Fail', error })
  }
}

module.exports = verifyToken