module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  rules: {
    semi: ['error', 'never'],
    'comma-dangle': ['error', 'never'],
    indent: ['error', 2],
    'no-irregular-whitespace': ['error', {
      skipStrings: true,
      skipComments: true,
      skipRegExps: true,
      skipTemplates: true
    }],
    'prefer-const':'error',
    'no-multiple-empty-lines': ['error', {
      max: 1,
      maxEOF: 0
    }],
    'no-underscore-dangle': 'off',
    'class-methods-use-this': 'off',
    'max-len': [2, {
      code: 1000,
      ignorePattern: '^import .*'
    }],
    'space-infix-ops': ['error', { int32Hint: false }],
    'space-before-function-paren': ['error', {
      anonymous: 'always',
      named: 'always',
      asyncArrow: 'always'
    }],
    'keyword-spacing': ['error', {
      before: true,
      after: true
    }],
    'object-curly-spacing': ['error', 'always'],
    quotes: ['error', 'single', { allowTemplateLiterals: true }]
  }
}