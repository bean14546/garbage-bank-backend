const db = require('../config/db')

const TransactionHistorySchema = db.Schema({
  user: { type: db.Schema.Types.ObjectId, ref: 'users' },
  transaction: [{
    garbages: { type: db.Schema.Types.ObjectId, ref: 'garbages' },
    transaction_history_weight: { type: Number, required: true },
    transaction_history_total: { type: Number, required: true },
    transaction_history_date: { type: Date, dateFormat: 'YYYY-MM-DDTHH:mm:ss.sssZ', default: () => new Date() }
  }],
  is_paid: { type: Boolean, default: false, required: true },
  summary_price: { type: Number, default: 0, required: true },
  summary_weight: { type: Number, default: 0, required: true },
  created_at: { type: Date, default: () => new Date() },
  updated_at: { type: Date, default: () => new Date() },
  __v: { type: Number, select: false }
})

const TransactionHistoryModel = db.model('transaction_histories', TransactionHistorySchema)

module.exports = { TransactionHistoryModel }
