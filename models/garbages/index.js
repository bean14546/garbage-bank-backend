const db = require('../../config/db')

const GarbageSchema = db.Schema({
  garbage_image: { type: Array, required: true },
  garbage_name: { type: String, required: true },
  garbage_price: { type: Number, required: true },
  garbage_details: { type: String, required: true },
  garbage_category: { type: db.Schema.Types.ObjectId, ref: 'garbage_categories' },
  __v: { type: Number, select: false }
})

const GarbageModel = db.model('garbages', GarbageSchema)

module.exports = { GarbageModel }
