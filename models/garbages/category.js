const db = require('../../config/db')

const GarbageCategorySchema = db.Schema({
  garbage_category_image: { type: Array, required: true },
  garbage_category_name: { type: String, required: true },
  garbage_category_detail: { type: String, required: true },
  garbages: [{ type: db.Schema.Types.ObjectId, ref: 'garbages' }],
  __v: { type: Number, select: false }
})

const GarbageCategoryModel = db.model('garbage_categories', GarbageCategorySchema)

module.exports = { GarbageCategoryModel }