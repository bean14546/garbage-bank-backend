const express = require('express')
const router = express.Router()
const userController = require('../../controllers/users/index')

router.route('/').get(userController._find)
router.route('/:id').get(userController._findByID)
router.route('/:id').put(userController._update)
router.route('/:id').delete(userController._delete)

module.exports = router