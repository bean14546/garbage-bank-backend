const express = require('express')
const router = express.Router()
const authController = require('../../controllers/users/auth')

router.route('/register').post(authController._register)
router.route('/login').post(authController._login)
router.route('/logout').post(authController._logout)
router.route('/checkToken').post(authController._checkToken)

module.exports = router