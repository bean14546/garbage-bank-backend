const express = require('express')
const router = express.Router()
const transactionHistoryController = require('../controllers/transactionHistory')

router.route('/').get(transactionHistoryController._find)
router.route('/:id').get(transactionHistoryController._findByID)
router.route('/').post(transactionHistoryController._create)
router.route('/:id').put(transactionHistoryController._update)
router.route('/:id').delete(transactionHistoryController._delete)

module.exports = router