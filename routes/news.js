const express = require('express')
const router = express.Router()
const newsController = require('../controllers/news')

router.route('/').get(newsController._find)
router.route('/:id').get(newsController._findByID)
router.route('/').post(newsController._create)
router.route('/:id').put(newsController._update)
router.route('/:id').delete(newsController._delete)

module.exports = router