// Require Package
const express = require('express')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const createError = require('http-errors')
const path = require('path')
const dotenv = require('dotenv')
dotenv.config({ path: '.env.dev' })

// Declare Variable
const { APP_URL, APP_PORT, ALLOW_ORIGIN, ALLOW_METHODS } = process.env
const corsOptions = { origin: `${ALLOW_ORIGIN}`, methods: `${ALLOW_METHODS}` }
const app = express()

app.use(cors(corsOptions))
app.use(logger('dev'))
app.use(express.json())

app.use(require('./routes'))
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
// catch 404 and forward to error handler
app.use((req, res, next) => next(createError(404)))
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

app.listen(APP_PORT, APP_URL, () => {
  console.log(`Your Application is running on ${APP_PORT}`)
})

module.exports = app
